jQuery.fn.multitabs = function () {
    var _this = jQuery(this);

    jQuery('.tab__header > div').on('click', function () {
        var num;
        var _this = jQuery(this);
        var classNameTab = _this.attr('class').split(' ');
        var classNameContent = _this.parent().siblings().children();

        for (i = 0; i <= classNameTab.length; i++) {
            if (/([\d.]+)/.test(classNameTab[i])) {
                num = classNameTab[i].split('-')[1];
                _this.siblings().removeClass('tab__header--active');
                _this.addClass('tab__header--active');
            }
        }

        jQuery(classNameContent).each(function (i, n) {
            var name = jQuery(n).attr('class').split(' ');
            for (n = 0; n <= name.length; n++) {
                if (name[n] != undefined) {
                    if (/([\d.]+)/.test(name[n])) {
                        if (num === name[n].split('-')[1]) {
                            var el = jQuery(classNameContent)[num - 1];
                            jQuery(classNameContent).removeClass('tab__content--active');
                            jQuery(el).addClass('tab__content--active');
                        }
                    }
                }
            }
        });

    });

}