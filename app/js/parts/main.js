jQuery(document).ready(function($) {
	// global


	transformicons.add('.tcon');
	$('.tel, #phone, input[name="phone"]').mask('+7 (999) 999-9999');
	$('select').niceSelect();
	$('.empty-link').click(function (e) {
		e.preventDefault();
	});
	$('input').styler({
		selectSearch: true,
	});

	if ($(window).width() < 1024) {
		$('.mobile-wrap').wrapAll('<div class="header-mobile-wrap"></div>');
	}

	$('.header-burger').click(function() {
		$('.header-mobile-wrap').slideToggle(); 
	});
    
    // main
    
    $(".anchor-animate").click(function (e) {
        e.preventDefault();
        var a = $("[data-anchor='" + $(this).attr("href").replace("#", "") + "']");
        $("html, body").animate({
            scrollTop: (a.offset().top - 300) + 'px'
        });
    });
    
    (function($){
    
        $(document).on('click', '.js_show', function(){
            $('#basicModalContent').addClass('show');
        });
        $(document).on('click', '.modalLogin__closeButton', function(){
            $('#basicModalContent').removeClass('show');
        });

        $('#sendPhone').on('submit', function(event){
            event.preventDefault();

            var data = $(this).serialize();

            $('.error_msg').remove();
            $('.error').removeClass('error');

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: data,
                success: function(response){
                    if (!response.success){
                        for (field in response.data){
                            var input = $('input[name="'+field+'"]');

                            input.addClass('error');
                            input.after('<div class="error_msg">'+response.data[field]+'</div>');
                        }
                    } else {
                        $('#sendPhone').hide();

                        $('input[name="login"]').val($('input[name="phone"]').val());
                        $('#tryLogin').show();
                    }
                }
            })
        })

        $('#tryLogin').on('submit', function(event){
            event.preventDefault();

            var data = $(this).serialize();

            $('.error_msg').remove();
            $('.error').removeClass('error');

            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: data,
                success: function(response){
                    if (!response.success){
                        for (field in response.data){
                            var input = $('input[name="'+field+'"]');

                            input.addClass('error');
                            input.after('<div class="error_msg">'+response.data[field]+'</div>');
                        }
                    } else {
                        location.href = response.data.link;
                    }
                }
            })
        })

    })(jQuery)
    
});