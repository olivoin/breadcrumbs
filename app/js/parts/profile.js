var $Common = function (dateRules) {
	var head = document.head || document.getElementsByTagName('head')[0],
		style = document.createElement('style'),
		styleSheet,
		_class;

	head.appendChild(style);
	styleSheet = document.styleSheets[document.styleSheets.length - 1];

	_class = {
		getType: function (target) {
			var dataType = [
					'String',
					'Number',
					'Boolean',
					'Array',
					'Function',
					'Date',
					'RegExp',
					'Error',
					'Symbol'
				],
				nodeType = [
					'Element',
					'Attribute',
					'Text Node',
					'CDATA Section',
					'Entity Reference',
					'Entity',
					'Processing Instruction',
					'Comment',
					'Document',
					'Document Type',
					'Document Fragment',
					'Notation'
				];

			if (target === undefined || target === null) {
				return target;
			} else {
				var type = typeof target;

				if (type != 'object') {
					return type.slice(0, 1).toUpperCase() + type.slice(1);
				} else {
					type = Object.prototype.toString.call(target);

					if (type == '[object Object]') {
						type = 'Object';
						dataType.some(function (value) {
							var constructor = window[value];

							if (constructor && constructor.prototype && target instanceof constructor) {
								type = type;
								return true;
							}
						});
						return type;
					} else {
						if (target instanceof Node && target.nodeType) {
							return nodeType[target.nodeType - 1];
						} else {
							type = type.match(/([\w\.]+)[^\w]*$/);

							if (type && dataType.indexOf(type[1]) >= 0) {
								return type[1];
							} else {
								return 'Unknown Type';
							}
						}
					}
				}
			}
		},

		clone: function (currentObject) {
			var newObject = new currentObject.constructor();

			Object.keys(currentObject).forEach(function (name) {
				newObject[name] = currentObject[name];
			});

			for (var i = 1; i < arguments.length; i++) {
				var addObject = arguments[i];

				Object.keys(addObject).forEach(function (name) {
					newObject[name] = addObject[name];
				});
			}

			return newObject;
		}
	};

	return _class;
}();

var $Mediator = function () {
	var _class,
		eventsList = {},
		lastEventsList = {};

	_class = {
		on: function () {
			checkHandlers({
				args: Array.prototype.slice.call(arguments),
				prepare: addHandlers
			});
			return this;
		},

		one: function () {
			checkHandlers({
				args: Array.prototype.slice.call(arguments),
				once: true,
				prepare: addHandlers
			});
			return this;
		},

		off: function () {
			checkHandlers({
				args: Array.prototype.slice.call(arguments),
				once: true,
				prepare: removeHandlers
			});
			return this;
		},

		fire: function (e) {
			var args = Array.prototype.splice.call(arguments, 1),
				commonHandler = eventsList['*'],
				names,
				result = [];

			switch ($Common.getType(e)) {
				case 'String':
				case 'Array':
					names = e;
					break;
				case 'Object':
					names = e.name;
					break;
				default:
					return;
			}

			checkValues(names, function (name) {
				var event = getEvent(name),
					_event,
					_args,
					removeList = [];

				switch ($Common.getType(e)) {
					case 'Object':
						_event = $Common.clone(e);
						break;
					default:
						_event = {};
						break;
				}

				_event.name = name;
				_args = [_event].concat(args);
				lastEventsList[name] = _args;

				if (!event) {
					return;
				}

				event.progress = true;

				if (commonHandler) {
					var item = commonHandler.handlers[0];

					runHandler(item.handler, item.context || e.context || null, _args);
				}

				execute(0);

				function execute (index) {
					for (var i = index; i < event.handlers.length; i++) {
						var item = event.handlers[i],
							context = item.context || e.context || null;

						if (e.type && item.type != e.type) {
							continue;
						}

						if (item.once) {
							removeList.push(i);
						}

						if (item.async) {
							runHandler(item.handler, context, _args).then(function (_result) {
								if (item.report) {
									result.push(_result);
								}

								execute(i + 1);
							});
							break;
						} else {
							if (item.report) {
								result.push(runHandler(item.handler, context, _args));
							} else {
								runHandler(item.handler, context, _args);
							}
						}
					}

					if (result.length == event.reportCount && e.callback) {
						e.callback.apply(e.context || null, result);
					}

					if (i == event.handlers.length) {
						for (var i = removeList.length; i--;) {
							if (event.handlers[i].report) {
								event.reportCount--;
							}

							event.handlers.splice(i, 1);
						}

						delete event.progress;
					}
				}
			});

			return this;
		}
	};

	return _class;

	function runHandler (handler, context, args) {
		return handler.apply(context, args);
	}

	function checkHandlers (options) {
		if (!options.args) {
			return;
		}

		switch ($Common.getType(options.args[0])) {
			case 'String':
				options.prepare({
					name: options.args[0],
					handlers: options.args.slice(1)
				}, options.once);
				break;
			case 'Object':
				options.args.forEach(function (item) {
					options.prepare(item, options.once);
				});
				break;
		}
	}

	function addHandlers (item, once) {
		var handlers = item.handlers;

		checkValues(item.name, function (name) {
			var event = setEvent(name);

			checkValues(handlers, function (execute) {
				if (check(execute.handler || execute)) {
					return;
				}

				var data = set([
							'type',
							'async',
							'report',
							'track'
						], item, {
						context: item.context || null,
						order: item.order || 10,
						once: once
					});

				switch ($Common.getType(execute)) {
					case 'Object':
						set([
							'handler',
							'type',
							'context',
							'order',
							'async',
							'report',
							'track'
						], execute, data);
						break;
					case 'Function':
						data.handler = execute;
						break;
				}

				setHandler(name, data);

				if (data.track && lastEventsList[name] && !event.progress) {
					runHandler(data.handler, data.context, lastEventsList[name]);
				}

				function check (handler) {
					return event.handlers.some(function (item) {
						if (handler == item.handler && item.context == item.context) {
							return true;
						}
					});
				}

				function set (names, source, target) {
					names.forEach(function (name) {
						if (source[name] !== undefined) {
							target[name] = source[name];
						}
					});
					return target;
				}
			});
		});
	}

	function removeHandlers (item) {
		checkValues(item.name, function (name) {
			if (item.handlers) {
				checkValues(item.handlers, function (handler) {
					var options = {
							handler: handler
						};

					if (item.context) {
						options.context = item.context;
					}

					resetHandlers(name, options);
				});
			} else if (item.context) {
				resetHandlers(name, {
					context: item.context
				});
			} else {
				resetEvent(name);
			}
		});
	}

	function getEvent (name) {
		return eventsList[name];
	}

	function setEvent (name) {
		if (!eventsList[name]) {
			eventsList[name] = {
				handlers: [],
				reportCount: 0
			};
		}

		return eventsList[name];
	}

	function resetEvent (name) {
		delete eventsList[name];
	}

	function resetHandlers (name, options) {
		var event = eventsList[name];

		if (!event) {
			return;
		}

		var handlers = getHandlers(name);

		if (!handlers || !handlers.length) {
			return;
		}

		var keys = Object.keys(options);

		for (var i = handlers.length ; i--;) {
			if (check(handlers[i])) {
				if (handlers[i].report) {
					event.reportCount--;
				}

				handlers.splice(i, 1);
			}
		}

		function check (item) {
			return keys.every(function (key) {
				return options[key] == item[key];
			});
		}
	}

	function getHandlers (name) {
		if (eventsList[name]) {
			return eventsList[name].handlers;
		}
	}

	function setHandler (name, data) {
		var event = eventsList[name];

		if (event) {
			if (name == '*') {
				if (!event.handlers.length) {
					event.handlers.push(data);
				}
			} else {
				event.handlers.push(data);

				if (data.report) {
					event.reportCount++;
				}

				event.handlers.sort(function (a, b) {
					return a.order - b.order;
				});
			}
		}
	}

	function checkValues (values, callback) {
		switch ($Common.getType(values)) {
			case 'String':
				values = values.split(/\s+/);
			case 'Array':
				values.forEach(function (value) {
					callback(value);
				});
				break;
			case 'Function':
				callback(values);
				break;
		}
	}
}();

jQuery(document).ready(function ($) {
	var $Profile,
		$Schedule;

	function Component (selector) {
		var _class = this,
			wrapper = $(selector);

		if (wrapper.length) {
			$.extend(this, {
				wrapper: wrapper,
				templates: Object.create(null)
			});

			if (wrapper.data('settings')) {
				$.extend(this, {
					settings: wrapper.data('settings')
				});
			}

			$('[data-template-id]', wrapper).each(function () {
				var template = $(this);

				_class.templates[template.data('template-id')] = template.text().trim()
					.replace(/>\s+</g, '><')
					.replace(/\}\s+</g, '}<')
					.replace(/>\s+\{/g, '>{');
			});
		}
	}

	Component.prototype = Object.create(null);

	$Profile = function () {
		var _class = new Component('[data-wrapper="profile"]');

		if (!_class.wrapper) {
			return;
		}

		function Field (wrapper) {
			var field = this,
				input = $('input', wrapper);

			if (input.length) {
				switch (input.attr('type')) {
					case 'tel':
					case 'email':
					case 'text':
						input.on('input', this.checkValue.bind(this));
						break;
				}

				$.extend(this, {
					input: input,
					name: input.attr('name'),
					defaultValue: input.val(),
					currentValue: input.val()
				});
			}

			if (wrapper.data('required')) {
				$.extend(this, {
					require: true
				});
			}

			$.extend(this, {
				wrapper: wrapper
			});
		}

		Field.prototype = {
			checkValue: function () {
				this.currentValue = this.input.val();

				if (this.currentValue == this.defaultValue) {
					_class.checkModify();
				} else {
					_class.setSaveMode();
				}
			},

			checkError: function () {

			},

			setError: function (message) {
				this.error = $(_class.templates.fieldError
					.replace('{{message}}', message)
				).appendTo(this.wrapper);
			},
            
            removeError: function() {
                this.wrapper.find('.field-error-wrapper').remove();
            }
		};

		$.extend(_class, {
			fieldWrapper: $('[data-wrapper="fields"]', _class.wrapper),
			items: {},

			checkModify: function () {
				var status = Object.keys(_class.items).some(function (name) {
						var field = _class.items[name];
						if (field.currentValue && field.currentValue != field.defaultValue) {
							return true;
						}
					});
                if (jQuery('[name="old_password"]').val() || jQuery('[name="new_password"]').val() || jQuery('[name="new_password_check"]').val()){
                    status = true;
                }
				if (status) {
					this.setSaveMode();
				} else {
					this.setEditMode();
				}
			},

			setEditMode: function () {
				this.wrapper.attr('data-mode', 'edit');

				if (_class.fieldWrapper.prop('disabled')) {
					_class.fieldWrapper.prop('disabled', false);
				}
			},

			setSaveMode: function () {
				this.wrapper.attr('data-mode', 'save');

				if (_class.fieldWrapper.prop('disabled')) {
					_class.fieldWrapper.prop('disabled', false);
				}
			},

			setDisableMode: function () {
				this.wrapper
					.removeData('mode')
					.removeAttr('data-mode');
				this.fieldWrapper.prop('disabled', true);
				Object.keys(_class.items).forEach(function (name) {
					var field = _class.items[name];

					field.defaultValue = field.currentValue;
					field.input.get(0).defaultValue = field.currentValue;

					if (field.error) {
						field.error.remove();
						delete field.error;
					}
				});
			},

			saveData: function () {
                Object.keys(_class.items).forEach(function(name){
                    _class.items[name].removeError();
                });
				$.ajax({
					url: this.wrapper.attr('action'),
					method: 'POST',
					dataType: 'json',
					data: this.wrapper.serialize()
				}).done(function (response) {
					if (response.success) {
						_class.setDisableMode();
					} else {
						if (response.data) {
							Object.keys(response.data).forEach(function (name) {
								_class.items[name].setError(response.data[name]);
							});
							// _class.setErrorMode();
						}
					}
				}).fail(function (xhr, error, status) {
					console.log(error, status);
				});
			}
		});

		$('[data-item="profile"]', _class.fieldWrapper).each(function () {
			var field = new Field($(this));

			if (field.name) {
				_class.items[field.name] = field;
			}
		});

		_class.wrapper
			.on('click', '[data-action="edit-profile"]', function () {
				var button = $(this);

				if (_class.wrapper.data('mode')) {
					_class.saveData();
				} else {
					_class.setEditMode();
				}
			})
			.on('reset', _class.setDisableMode.bind(_class));

		return _class;
	}();

	$Schedule = function (initOptions) {
		var root = $(document.documentElement),
			_class = new Component('[data-wrapper="schedule"]');

		if (!_class.wrapper) {
			return;
		}

		function Day (id, data, rangeSettings) {
			var item = this,
				time = data.time ? getStringTime(data.time) : '',
				time2 = data.time2 ? getStringTime(data.time2) : '...',
				count = 0,
				wrapper = $(_class.templates.dayTemplate
					.replace('{{id}}', data.id)
					.replace('{{name}}', data.name)
					.replace('{{time}}', time)
					.replace('{{time2}}', time2));

			$.extend(this, {
				wrapper: wrapper,
				id: id,
				name: data.name,
				time: data.time,
				productsWrapper: $('[data-wrapper="products"]', wrapper),
				priceItem: $('[data-item="day-price"]', wrapper),
				timeItem: $('[data-item="time"]', wrapper),
				timeItemEnd: $('[data-item="time2"]', wrapper),
				products: {}
			});

			if (data.items) {
				data.items.forEach(this.add.bind(this));
				this.setCount(data.items.length);
				this.setPrice();
			} else {
				this.setCount(0);
			}

			this.createSlider(rangeSettings, data.time, data.time2);

			wrapper
				.on('click', '[data-action="time-change"]', function () {
					item.wrapper.attr('data-mode', 'edit');
				})
				.on('click', '[data-action="add"]', function () {
					$Mediator.fire('product:popup', id, Object.keys(item.products));
				});
		}

		Day.prototype = {
			createSlider: function (data, time, time2) {
				var item = this,
					width = 100 / data.totalCount,
					items = [],
					start = 0,
					end = 15;

				if (time) {
					start = Math.round(((time.hours * 60 + time.minutes) - data.minValue.time) / data.delta);
				}
				if(time2){
					end = Math.round(((time2.hours * 60 + time2.minutes) - data.minValue.time) / data.delta);
				}

				$.extend(this, {
					range: $('[data-item="time-slider"]', this.wrapper)
				});

				this.currentTime = getTime(start);
				this.currentTimeEnd = getTime(end);
				this.currentValue = start;
				this.currentValueEnd = end;
				this.rangeWrapper = $('[data-wrapper="date"]', this.wrapper);
				this.newTimeItem = $('[data-item="new-time"]', this.rangeWrapper)
					.text(getStringTime(this.currentTime));

				this.newTimeItemEnd = $('[data-item="new-time-end"]', this.rangeWrapper)
					.text(getStringTime(this.currentTimeEnd));

				for (var i = 0; i <= data.totalCount; i++) {
					var currentTime = data.minValue.time + data.delta * i,
						currentItem;

					if (currentTime % data.hourCount) {
						currentItem = getItem(_class.templates.rangeMinutesItem, data.delta * i);
					} else {
						currentItem = getItem(_class.templates.rangeHoursItem, Math.floor((currentTime / 60)));
					}

					currentItem.css({
						width: width + '%',
						left: 100 * (i / data.totalCount) + '%'
					});
					items.push(currentItem);
				}

				$('[data-item="time-list"]', this.wrapper).append(items);
				noUiSlider.create(this.range.get(0), {
					start: [start, end],
                    connect: true,
					step: 1,
					range: {
						'min': 0,
						'max': data.totalCount
					}
				});
				// this.range.get(0).noUiSlider.on('change', function () {
				// 	var value = parseInt(this.get());

				// 	item.newValue = value;
				// 	item.newTime = getTime(value);
				// });
				this.range.get(0).noUiSlider.on('slide', function (ev) {
					
					var value = parseInt(this.get()[0]),
						value2 = parseInt(this.get()[1]),
						time = getTime(value),
						time2 = getTime(value2);
						item.newValue = value;
						item.newValue2 = value2;
						item.newTime = time;
						item.newTime2 = time2;
						//set times on workspace
						item.timeItem.text(getStringTime(time));
						item.timeItemEnd.text(getStringTime(time2));
						//set times on modal
						item.newTimeItem.text(getStringTime(time));
						item.newTimeItemEnd.text(getStringTime(time2));

					if (value != item.currentValue||value2 != item.currentValueEnd) {
						item.rangeWrapper.attr('data-mode', 'save');
					} else {
						item.rangeWrapper
							.removeData('mode')
							.removeAttr('data-mode');
					}
				});
				

				// this.range
				// 	.on('mousewheel MozMousePixelScroll', function (e) {
				// 		var direction = e.originalEvent.deltaY || e.originalEvent.detail,
				// 			value = item.newValue || item.currentValue;

				// 		e.preventDefault();

				// 		if (direction > 0) {
				// 			if (value < data.totalCount - 1) {
				// 				value++;
				// 			} else {
				// 				return;
				// 			}
				// 		} else {
				// 			if (value > 0) {
				// 				value--;
				// 			} else {
				// 				return;
				// 			}
				// 		}

				// 		var time = getTime(value);

				// 		item.newValue = value;
				// 		item.newTime = time;
				// 		item.newTimeItem.text(getStringTime(time));
				// 		item.range.get(0).noUiSlider.set(value);

				// 		if (value != item.currentValue) {
				// 			item.rangeWrapper.attr('data-mode', 'save');
				// 		} else {
				// 			item.rangeWrapper
				// 				.removeData('mode')
				// 				.removeAttr('data-mode');
				// 		}
				// 	});

				this.rangeWrapper
					.on('click', '[data-action="save-time"]', function () {
						//console.log(item)
						item.currentTime = item.newTime;
						item.currentValue = item.newValue;

						item.currentTimeEnd = item.newTime2;
						item.currentValueEnd = item.newValue2;

						item.timeItem.text(getStringTime(item.newTime));
						item.timeItemEnd.text(getStringTime(item.newTime2));

						item.newTimeItem.text(getStringTime(item.newTime));
						item.newTimeItemEnd.text(getStringTime(item.newTime2));
                        
                        item.saveData({
                            action: 'change_time',
                            value: item.currentTime,
                            value2: item.currentTimeEnd,
                            dayId: item.id,
                        });
						reset();
					})
					.on('click', '[data-action="reset-time"]', function () {
						//console.log(item.range.get(0).noUiSlider.get())
						item.range.get(0).noUiSlider.set([item.currentValue,item.currentValueEnd]);
						
						item.timeItem.text(getStringTime(item.currentTime));
						item.timeItemEnd.text(getStringTime(item.currentTimeEnd));

						item.newTimeItem.text(getStringTime(item.currentTime));
						item.newTimeItemEnd.text(getStringTime(item.currentTimeEnd));


						//item.timeItemEnd.text(getStringTime(item.currentTimeEnd));

						// item.newTimeItem.text(getStringTime(item.currentTime));
						// item.newTimeItemEnd.text(getStringTime(item.currentTimeEnd));
						reset();
					});

				function reset () {
					delete item.newValue;
					delete item.newTime;
					item.wrapper
						.removeData('mode')
						.removeAttr('data-mode');
					item.rangeWrapper
						.removeData('mode')
						.removeAttr('data-mode');
				}

				function getTime (value) {
					var count = value ? parseInt(value) : 0,
						currentTime = data.minValue.time + parseInt(count) * data.delta,
						hours = Math.floor(currentTime / 60),
						minutes = currentTime % 60;

					return {
						hours: hours,
						minutes: minutes
					};
				}

				function getItem (template, value) {
					return $(template.replace(/\{\{value\}\}/g, value));
				}
			},

			add: function (data) {
				var item = this;

				if (Array.isArray(data)) {
					data.forEach(function (productData) {
						if (item.products[productData.id]) {
							return;
						}

						addProduct(productData);
					});
				} else {
					addProduct(data);
				}

				this.setCount(Object.keys(this.products).length);
				this.setPrice();

				function addProduct (productData) {
					var productItem = new Product(item.id, productData);

					item.products[productData.id] = productItem;
					item.productsWrapper.append(productItem.wrapper);
				}
			},

			remove: function (id) {
				delete this.products[id];
				this.setCount(Object.keys(this.products).length);
				this.setPrice();
			},

			addProduct: function (productData) {

			},

			changeProduct: function (productId, value) {
				this.setPrice(value);
				this.saveData({
					action: 'change_qty',
					dayId: this.id,
					productId: productId,
					value: value
				});
			},

			removeProduct: function (productId) {
				this.remove(productId);
				this.saveData({
					action: 'delete_product',
					dayId: this.id,
					productId: productId
				});
			},

			setCount: function (value) {
				this.wrapper.removeData('count');

				if (value) {
					this.wrapper.attr('data-count', value);

					if (value == _class.productCount) {
						this.wrapper.attr('data-full', true);
					} else {
						this.wrapper
							.removeData('full')
							.removeAttr('data-full');
					}
				} else {
					this.wrapper.removeAttr('data-count');
				}
			},

			setPrice: function () {
				var item = this,
					price = 0;

				Object.keys(this.products).forEach(function (id) {
					var product = item.products[id];

					price += product.count * product.price;
				});

				price = price || '';
				this.priceItem.text(price);
			},

			saveData: function (sendData) {
				$.ajax({
					url: _class.settings.saveRequest,
					method: 'POST',
					dataType: 'json',
					data: sendData
				}).done(function (data) {
					
				}).fail(function (xhr, error, status) {
					console.log(error, status);
				});
			}
		};

		function Product (parentId, data) {
			var item = this,
				wrapper = $(_class.templates.productTemplate
					.replace('{{id}}', data.id)
					.replace('{{name}}', data.name)
					.replace('{{image}}', data.image)
					.replace('{{type}}', data.type)
					.replace('{{weight}}', data.weight)
					.replace('{{length}}', data.length)
					.replace('{{price}}', data.price)
					.replace('{{count}}', data.count || 1));

			$.extend(this, {
				wrapper: wrapper,
				id: data.id,
				parentId: parentId,
				count: data.count || 1,
				price: data.price,
				countItem: $('[data-item="count"]', wrapper),
				totalPrice: $('[data-item="total-price"]', wrapper)
			});

			this.setPrice();
			this.countItem
				.styler()
				.on('input', function () {
					var value = $(this).val();

					if (item.count != value) {
						item.count = value;
						item.setPrice();
						$Mediator.fire('product:change', parentId, item.id, value);
					}
				});

			wrapper
				.on('click', '[data-action="remove"]', function () {
					$Mediator.fire('product:remove', parentId, item.id);
					item.wrapper.remove();
				});
		}

		Product.prototype = {
			setPrice: function () {
				this.totalPrice.text(this.count * this.price);
			}
		};

		$.extend(_class, {
			items: {},

			getData: function () {
				return $.ajax({
					url: this.settings.getRequest,
					method: 'POST',
					dataType: 'json'
				});
			},

			createItems: function (data, rangeSettings) {
				var items = [];

				for (var index = 0; index < 7; index++) {
					var dayId = 'd' + (index + 1),
						dayParams = {
							name: initOptions.days[index],
							id: dayId,
							settings: this.settings
						},
						dayData = data.days[dayId],
						dayItem;

					if (dayData) {
						$.extend(dayParams, {
							items: Object.keys(dayData.list).map(function (id) {
								return $.extend({}, data.products[id], {
									id: id,
									count: dayData.list[id]
								});
							}),
							time: dayData.time,
							time2: dayData.time_end?dayData.time_end:false
						});
					}

					dayItem = new Day(dayId, dayParams, rangeSettings);
					this.items[dayId] = dayItem;
					items.push(dayItem.wrapper);
				}

				this.wrapper.append(items);
			}
		});

		_class.popupWrapper = function () {
			var _component = Object.create(null),
				wrapper = $(_class.templates.productsPopup).appendTo($(document.body));

			$.extend(_component, {
				wrapper: wrapper,
				list: $('[data-wrapper="products"]', wrapper),
				dayTitle: $('[data-item="day-title"]', wrapper),
				items: {},
                
				openPopup: function (dayItem) {
                    console.log('Здесь показывается модалка, но мы ее спрятали, т.к. верстки на нее нет. Раскомментируйте строку чуть ниже этого комментария в файле profile.js и верстайте');
					// root.attr('data-mode', 'popup');
					this.dayTitle.text(dayItem.name);
				},
				closePopup: function () {
					var dayItem = _class.items[this.activeId];

					dayItem.add(Object.keys(_component.selectedProducts).map(function (id) {
						return $.extend({}, _class.products[id], {
							id: id
						});
					}));
					delete _component.activeId;
					delete _component.selectedProducts;
					root
						.removeData('mode')
						.removeAttr('data-mode');
				},
                saveData: function (sendData) {
                    $.ajax({
                        url: _class.settings.saveRequest,
                        method: 'POST',
                        dataType: 'json',
                        data: sendData
                    }).done(function (data) {
                        // location.reload();
                    }).fail(function (xhr, error, status) {
                        console.log(error, status);
                    });
                },
			});

			if (wrapper.data('settings')) {
				$.extend(_component, {
					settings: wrapper.data('settings')
				});
			}

			wrapper
				.on('click', '[data-action="close"]', function () {
					_component.closePopup();
				})
				.on('click', '[data-action="add"]', function () {
					var item = $(this).closest('[data-item="product"]'),
						id = item.data('id');
					_component.selectedProducts[id] = true;
					item.attr('data-selected', true);
                    
                    _component.saveData({
                        action: 'add_product',
                        productId: id,
                        dayId: _component.activeId,
                    });
				});

			$Mediator
				.on('product:popup', function (e, dayId, productsList) {
					var dayItem = _class.items[dayId],
						items = [];

					_component.selectedProducts = {};
					_component.activeId = dayId;
					_component.openPopup(dayItem);

					Object.keys(_class.products).forEach(function (id) {
						if (!dayItem.products[id]) {
							// item.attr('data-selected', true);

							var productData = _class.products[id],
								item = $(_class.templates.productsPopupItem
									.replace('{{id}}', id)
									.replace('{{name}}', productData.name)
									.replace('{{image}}', productData.image)
									.replace('{{type}}', productData.type)
									.replace('{{price}}', productData.price)
									.replace('{{weight}}', productData.weight)
									.replace('{{length}}', productData.length)
								);

							items.push(item);
						}
					});
                    
                    _component.list.empty();
                    for (var i = 0; i < items.length; i++) {
                         _component.list.append(items[i]);
					}
				});

			return _component;
		}();

		$Mediator
			.on('product:change', function (e, dayId, productId, value) {
				var dayItem = _class.items[dayId];

				if (dayItem) {
					dayItem.setPrice(value);
					dayItem.saveData({
						action: 'change_qty',
						dayId: dayId,
						productId: productId,
						value: value
					});
				}
			})
			.on('product:remove', function (e, dayId, productId) {
				var dayItem = _class.items[dayId];

				if (dayItem) {
					dayItem.remove(productId);
					dayItem.saveData({
						action: 'delete_product',
						dayId: dayId,
						productId: productId
					});
				}
			});

		_class.getData(_class.settings.getRequest).done(function (data) {
			if (data.success) {
				if (!_class.rangeSettings) {
					_class.rangeSettings = prepareData(_class.settings);
				}

				_class.products = data.products;
				_class.productCount = Object.keys(data.products).length;
				_class.createItems(data, _class.rangeSettings);
			}

			function prepareData (data) {
				var minValue = getTimeObject(data.deliveryMin),
					maxValue = getTimeObject(data.deliveryMax),
					delta = parseInt(data.deliveryDelta);

				return {
					minValue: minValue,
					maxValue: maxValue,
					delta: delta,
					totalCount: Math.round((maxValue.time - minValue.time) / delta),
					hourCount: Math.round(60 / delta)
				};

				function getTimeObject (value) {
					var values = value.split(':'),
						result = {
							hours: parseInt(values[0]),
							minutes: parseInt(values[1]) || 0
						};

					result.time = result.hours * 60 + result.minutes
					return result;
				}
			}
		}).fail(function (xhr, error, status) {
			console.log(error, status);
		});

		function getStringTime (item) {
			return [
				item.hours,
				('0' + item.minutes).slice(-2)
			].join(':');
		}

		return _class;
	}({
		days: [
			'Понедельник',
			'Вторник',
			'Среда',
			'Четверг',
			'Пятница',
			'Суббота',
			'Воскресенье'
		]
	});
});

// orders

(function($){
    var current_row = null;
    var $xhr = null;
    
    $(document).ready(function(){
        $('.time-picker input').timepicker({
            altFieldTimeOnly:	false,
            altTimeFormat:		'HH:mm:ss',
            showButtonPanel:	true,
            controlType: 		'select',
            oneLine:			true,
            change: function(time){

            }
        });
        $('.time-picker input').on('change', function() {
            var time = $(this).val();
            var index = $(this).data('index');
            
            $.ajax({
                url: ajaxurl,
                method: 'post',
                data: {
                    action: 'change_time',
                    time: time,
                    index: index,
                },
                success: function(response){
                    if (!response.success){
                        toastr.error(response.data.msg);
                    }
                }
            })
        });
        
        if ($('.js_product_list').length){
            $('.js_product_list').select2({
                ajax: {
                    url: ajaxurl,
                    method: 'post',
                    data: function(params){
                        var query = {
                            action: 'get_products',
                            s: params.term,
                        }
                        return query;
                    },
                    dataType: 'json',
                },
                templateResult: select2OptionFormat,
                templateSelection: select2OptionFormat,
                escapeMarkup: function(m) {return m; }
            });
        }
    });
    
    function select2OptionFormat(option) {
        var originalOption = option.element;
        if ($(originalOption).data('html')) {
            return $(originalOption).data('html');
        }

        return option.text;
    }
    
    $('.tab__content-1').append($('#profile-list-wrapper'));
    
    $('.js_add_product').on('click', function(){
        var modal = $('#productAddModal');
            modal.modal('show');
            
        var index = $(this).data('index');
        
        current_row = $(this).siblings('table').find('tbody');
        
        $('.js_field').remove();
        $('#productAddForm').append($('<input />', {type: 'hidden', name: 'field', value: index, className: 'js_field'}));
    });
    

    $('#productAddForm').on('submit', function(event){
        event.preventDefault()
        
        var data = $(this).serialize();
        $.ajax({
            url: ajaxurl,
            data: data,
            method: 'post',
            success: function(response){
                if (response.success){
                    current_row.append(response.data.html);
                    $('.js_order_total').html(response.data.total);
                } else {
                    toastr.error(response.data.msg);
                }
            }
        })
        
        $('#productAddModal').modal('hide');
    })
    
    
    $(document).on('click', '.js_delete_row', function(){
        var index = $(this).data('index');
        var row = $(this).closest('tr');
        var row_index = row.index() + 1;
        
        $.ajax({
            url: ajaxurl,
            method: 'post',
            data: {
                action: 'delete_product',
                index: index,
                row: row_index,
            },
            success: function(response){
                if (response.success){
                    row.remove();
                    $('.js_order_total').html(response.data.total);
                } else {
                    toastr.error(response.data.msg);
                }
            }
        });
    })
    
    $(document).on('change', '.js_item_qty', function(){
        var index = $(this).data('index');
        var price = $(this).data('price');
        var row = $(this).closest('tr');
        var row_index = row.index() + 1;
        var qty = $(this).val();
        
        if ($xhr){
            $xhr.abort();
        }
        $xhr = $.ajax({
            url: ajaxurl,
            method: 'post',
            data: {
                action: 'change_qty',
                index: index,
                row: row_index,
                qty: qty,
            },
            success: function(response){
                if (response.success){
                    row.find('.js_item_summ').text(price * qty);
                    $('.js_order_total').html(response.data.total);
                }
            }
        });
    })
})(jQuery)


// other

jQuery('.tabs').multitabs();