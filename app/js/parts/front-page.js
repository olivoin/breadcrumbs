jQuery(document).ready(function ($) {
    

    var containerEl = document.querySelector('.katalog-wrap');

    if (containerEl) {
        var mixer = mixitup('.katalog-wrap');
    }
    
    // cart
    
    $('.jq-number__spin.plus').on('click', function(){
        var item = $(this).closest('.katalog-table-item');
        item.block({
            message: null,
            overlayCSS: {
                opacity: .6
            }
        });
    });
    function updateQty(self, key,qty){
        var item = $(self).closest('.katalog-table-item');
        item.block({
            message: null,
            overlayCSS: {
                opacity: .6
            }
        });
        data = "cart_item_key="+key+"&cart_item_qty="+qty+"&action=decrease_qty";

        $.post( ajaxurl, data ).done(updateCartFragment);
    }
    function updateCartFragment(data) {
        if ( data && data.fragments ) {          
            $.each( data.fragments, function( key, value ) {
                $(key).replaceWith(value);
            });

            $('body').trigger( 'wc_fragments_refreshed' );
        }
    }
    
});