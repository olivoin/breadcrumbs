<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

$category_page = get_query_var('product_cat');
?>
<?php if (!is_front_page() && !$category_page): ?>
            </div>
        </div>
    </section>
</main>
<?php endif; ?>

<div id="basicModalContent" class="modal" role="dialog">
  <div role="document">
    <div class="phone_form">
        <form id="sendPhone" action="" method="post" class="modalLogin">            
            <div class="modalLogin__headerWrapper">
                <img src="<?=get_stylesheet_directory_uri()?>/dest/img/user.svg" class="modalLogin__headerUserImg">
                <p class="modalLogin__headerText">Вход в личный кабинет</p>
                <div class="modalLogin__closeWrapper">
                    <span>свернуть</span>
                    <button type="button" class="modalLogin__closeButton">-</button>
                </div>
            </div>
            
            <div class="modalLogin__main">
                <input type="hidden" name="action" value="send_phone">
                <p>Введите свой номер телефона</p>
                <input  class="modalLogin__mainInput" name="phone" id="phone" type="text" placeholder="+7" autofocus >
                <button class="modalLogin__mainBtn" type="submit">Отправить</button> 
                <img src="<?=get_stylesheet_directory_uri()?>/dest/img/right-arrow.jpg" class="modalLogin__mainArrow" >
            </div>
        </form>
        
        <form id="tryLogin" action="" method="post" style="display: none;" class="modalLogin">
            <div class="modalLogin__headerWrapper">
                <img src="<?=get_stylesheet_directory_uri()?>/dest/img/user.svg" class="modalLogin__headerUserImg">
                <p class="modalLogin__headerText">Вход в личный кабинет</p>
                <div class="modalLogin__closeWrapper">
                    <span>свернуть</span>
                    <button type="button" class="modalLogin__closeButton">-</button>
                </div>
            </div>
            
            
            <div class="modalLogin__main">
                <input type="hidden" name="action" value="try_login">
                <input type="hidden" name="login" type="hidden">

                <p>Пароль подтверждения</p>
                <input id="password" type="password" name="password"  class="modalLogin__mainInput" placeholder="Код из SMS">
                <p class="modalLogin__mainText">В течении 1 минуты придет смс с номером подтверждения</p>
                <button type="submit" class="modalLogin__mainBtn">Войти</button> 
                <img src="<?=get_stylesheet_directory_uri()?>/dest/img/right-arrow.jpg" class="modalLogin__mainArrow" >
            </div>
            
        </form>
    </div>
  </div>
</div>

<?php wp_footer(); ?>
<script src="<?=get_stylesheet_directory_uri()?>/dest/js/production.min.js"></script>
</body>
</html>
