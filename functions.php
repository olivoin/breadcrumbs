<?php


add_action( 'storefront_header', 'remove_parent_theme_stuff', 0 );
function remove_parent_theme_stuff() {
    if (is_user_logged_in()){
        remove_action( 'storefront_header', 'storefront_header_cart', 60 );
    }
}
add_action('storefront_page', 'remove_parent_theme_header_stuff', 0);
function remove_parent_theme_header_stuff() {
    remove_action( 'storefront_page', 'storefront_page_header', 10 );
}

if (is_user_logged_in()){
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
}


add_action( 'wp_enqueue_scripts', 'scripts', 999 );
function scripts(){
    
    //wp_enqueue_script( 'modal', get_stylesheet_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '4', true );
    //wp_enqueue_style('modal_style', get_stylesheet_directory_uri() . '/assets/css/modal.css');
    
    wp_localize_script( 'jquery', 'ajaxurl', admin_url('admin-ajax.php'));  // Create const for frontend
    //wp_enqueue_script( 'mask_input', get_stylesheet_directory_uri() . '/assets/js/jquery.maskedinput.js', array('jquery'), '4', true );
    //wp_enqueue_script( 'xleb-phone', get_stylesheet_directory_uri() . '/assets/js/modal.js', array('modal', 'mask_input'), '1.0', true );
    
    wp_enqueue_script('acf-timepicker', acf_get_url('assets/inc/timepicker/jquery-ui-timepicker-addon.min.js'), array('jquery-ui-datepicker'));
    wp_enqueue_style('acf-datepicker', acf_get_url('assets/inc/datepicker/jquery-ui.min.css'), '', '1.11.4' );
    wp_enqueue_style('acf-timepicker', acf_get_url('assets/inc/timepicker/jquery-ui-timepicker-addon.min.css'), '');
    
    //wp_enqueue_script( 'xleb-orders', get_stylesheet_directory_uri() . '/assets/js/orders.js', array('acf-timepicker', 'modal'), '1.0', true );
    
    wp_enqueue_style( 'xleb_toasrt_css' , '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css');
    wp_enqueue_script( 'xleb_toasrt_js' , '//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js', array('jquery'));

    /*Добавлено Егором верстка olivoin-khlebnye_kroshki*/
    //wp_enqueue_script( 'mixitup', get_stylesheet_directory_uri() . '/src/js/vendor/mixitup.js', array('jquery'), '4', true );
    //wp_enqueue_style( 'font', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=cyrillic' );
    //wp_enqueue_style( 'stl1', get_stylesheet_directory_uri() . '/assets/css/lib.css' );
    //wp_enqueue_style( 'style-prod', get_stylesheet_directory_uri() . '/dest/css/production.min.css' );

    //wp_enqueue_script( 'vendor', get_stylesheet_directory_uri() . '/assets/js/vendor.js', array('jquery'), '4', true );
    
    //wp_enqueue_script( 'main_js', get_stylesheet_directory_uri() . '/dest/js/production.min.js', array('jquery'), false );

    if (is_user_logged_in()){
        wp_deregister_script( 'storefront-sticky-add-to-cart' );
    }

    // Удаляем стили родительской темы
    wp_deregister_style('storefront-woocommerce-style');
    wp_deregister_style('storefront-style');
}
