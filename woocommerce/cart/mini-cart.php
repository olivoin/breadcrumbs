<?php
/**
 * Mini-cart
 *
 * Contains the markup for the mini-cart, used by the cart widget.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_mini_cart' ); ?>

<span class="header-meta-cart-title">корзина</span>
<span class="header-meta-cart-count"><?=WC()->cart->get_cart_contents_count();?></span>
<div class="header-cart-total-wrap">
    <div class="header-cart-wrap-box">
    
        <?php if ( ! WC()->cart->is_empty() ) : ?>
            <div class="header-cart-total">
                <?php
                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                        $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                        $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                            $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                            $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                            $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                            ?>
                            
                            <div class="katalog-table-item dis-flex flex-wrap-wrap align-items-center woocommerce-mini-cart-item <?php echo esc_attr( apply_filters( 'woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key ) ); ?>">
                                <div class="katalog-table-item-image col-lg-3">
                                    <?=$thumbnail;?>
                                </div>
                                
                                <div class="katalog-table-item-title col-lg-3">
                                    <?php if ( empty( $product_permalink ) ) : ?>
                                        <?php echo $product_name . '&nbsp;'; ?>
                                    <?php else : ?>
                                        <a href="<?php echo esc_url( $product_permalink ); ?>">
                                            <?php echo $product_name . '&nbsp;'; ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="katalog-table-item-count dis-flex align-items-center justify-content-center col-lg-4">
                                    <div class="katalog-table-item-count-price padding-r-6">
                                        <?=$product_price;?>
                                    </div>
                                    <div class="katalog-table-item-count-number">
                                        <div class="jq-number">
                                            <div class="jq-number__field">
                                                <input data-hash="number" 
                                                       class="quantity"
                                                       type="number" 
                                                       value="<?=$cart_item['quantity'];?>" 
                                                       min="1" 
                                                       step="1"
                                                       readonly>
                                            </div>
                                            <a href="<?=esc_url($_product->add_to_cart_url())?>" 
                                               data-quantity="1" 
                                               class="jq-number__spin plus product_type_simple add_to_cart_button ajax_add_to_cart" 
                                               data-product_id="<?=$cart_item['product_id']?>" 
                                               data-product_sku="<?=$_product->get_sku()?>"
                                               rel="nofollow">
                                            </a>
                                            <a class="jq-number__spin minus" 
                                               onClick="updateQty(this, '<?=$cart_item_key?>',<?=($cart_item['quantity']-1)?>)">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="katalog-table-item-count-summ padding-l-6">
                                        <?=wc_price($_product->get_price() * $cart_item['quantity']);?>
                                    </div>
                                </div>
                                
                                <div class="katalog-table-item-delete text-center col-lg-2">
                                    <?php
                                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                        '<a href="%s" class="remove_from_cart_button" aria-label="%s" data-product_id="%s" data-cart_item_key="%s" data-product_sku="%s">x</a>',
                                        esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                        __( 'Remove this item', 'woocommerce' ),
                                        esc_attr( $product_id ),
                                        esc_attr( $cart_item_key ),
                                        esc_attr( $_product->get_sku() )
                                    ), $cart_item_key );
                                    ?>
                                </div>
                                
                                <?php echo wc_get_formatted_cart_item_data( $cart_item ); ?>
                            </div>
                            <?php
                        }
                    }
                ?>
                <div class="header-cart-total-footer">
                    <div class="header-cart-total-footer-summ text-right col-lg-11 woocommerce-mini-cart__total total">
                        В сумме на <span data-headercart="total"><?php echo WC()->cart->get_cart_subtotal(); ?></span>
                    </div>
                    <div class="dis-flex justify-content-center">
                        <a href="#" class="button wc-forward js_show" data-headercart="checkout">Оформить заказ</a>
                    </div>
                </div>
            </div>
            </div>
        <?php else : ?>

            <p class="woocommerce-mini-cart__empty-message"><?php _e( 'No products in the cart.', 'woocommerce' ); ?></p>

        <?php endif; ?>

        <?php do_action( 'woocommerce_after_mini_cart' ); ?>

    </div>
</div>