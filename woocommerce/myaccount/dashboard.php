<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$customer_id = get_current_user_id();
$customer = get_userdata( $customer_id );
?>

<div class="profile-data">
    <div class="tabs">
        <div class="tab__header">
            <div class="tab__header-1 tab__header--active">
                <span class="tab__header-title">Ваш заказ</span>
            </div>
            <div class="tab__header-2">
                <span class="tab__header-title">Ваши данные</span>
            </div>
        </div>
        <div class="tab__content">
            <div class="tab__content-1 tab__content--active">
                
            </div>
            <div class="tab__content-2">
                <div class="dis-flex">
                    <div class="col-lg-8 margin-t-40">
                        <form action="<?=admin_url( 'admin-ajax.php' )?>?action=update_profile" class="profile-data-form" data-wrapper="profile">
                            <fieldset class="profile-data-field-wrapper" data-wrapper="fields" disabled>
                                <div class="dis-flex flex-wrap-wrap justify-content-end">
                                    <div class="col-lg-6 padding-r-30">
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile" data-required="true">
                                            <span class="profile-data-form-title">Вас зовут:</span>
                                            <input required type="text" name="full_name" value="<?=get_user_meta($customer_id, 'last_name', true) . ' ' . get_user_meta($customer_id, 'first_name', true)?>">
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile">
                                            <span class="profile-data-form-title">Ваш телефон:</span>
                                            <input required type="tel" name="phone" value="<?=get_user_meta($customer_id, 'billing_phone', true)?>" placeholder="+7 (123) 123-1231" readonly>
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile" data-required="true">
                                            <span class="profile-data-form-title">Ваша почта:</span>
                                            <input required type="email" name="email" value="<?=$customer->data->user_email?>">
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile">
                                            <span class="profile-data-form-title">Пароль:</span>
                                            <input type="password" name="old_password" value="">
                                        </label>
                                    </div>
                                    <div class="col-lg-6 padding-l-30" data-item="profile">
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile" data-required="true">
                                            <span class="profile-data-form-title">Район:</span>
                                            <input required type="text" name="state" value="<?=get_user_meta($customer_id, 'billing_state', true)?>">
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile" data-required="true">
                                            <span class="profile-data-form-title">Улица:</span>
                                            <input required type="text" name="address_1" value="<?=get_user_meta($customer_id, 'billing_address_1', true)?>">
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile" data-required="true">
                                            <span class="profile-data-form-title">Адрес:</span>
                                            <input required type="text" name="address_2" value="<?=get_user_meta($customer_id, 'billing_address_2', true)?>">
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile">
                                            <span class="profile-data-form-title">Новый пароль:</span>
                                            <input type="password" name="new_password" value="">
                                        </label>
                                        <label class="profile-data-item dis-flex flex-wrap-wrap align-items-center margin-b-40" data-item="profile">
                                            <span class="profile-data-form-title">Повторить пароль:</span>
                                            <input type="password" name="new_password_check" value="">
                                        </label>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="dis-flex flex-wrap-wrap justify-content-end">
                                <button type="button" class="profile-data-button button_edit button big" data-unactive="Редактировать" data-save="Сохранить" data-action="edit-profile"></button>
                                <button type="reset" class="profile-data-button button_reset button big">Отменить</button>
                            </div>
                            <script type="text/template" data-template-id="fieldError">
                                <span class="field-error-wrapper">{{message}}</span>
                            </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
