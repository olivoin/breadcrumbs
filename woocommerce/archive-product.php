<?php
/**
* The Template for displaying product archives, including the main shop page which is a post type archive
*
* This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.4.0
*/

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>
<main>
    <section id="front-hello">
        <div class="dis-flex full-height flex-wrap-wrap align-items-center justify-content-center">
            <div class="col-lg-8 col-md-11 wrap dis-flex flex-wrap-wrap align-items-center">
                <div class="front-hello-title col-lg-4 text-right">
                    <h1>Доставка<br>горячего хлеба<br>каждый день</h1>
                </div>
                <div class="front-hello-image col-lg-4">
                    <img src="<?=get_stylesheet_directory_uri()?>/dest/images/front/front-1.png">
                </div>
                <div class="front-hello-desc col-lg-4 text-block">
                    <p>Свежеиспеченный хлеб<br>на завтрак<br>в удобное для Вас время</p>
                    <a href="#katalog" class="button anchor-animate">первый заказ бесплатно</a>
                </div>
            </div>
        </div>
        <div class="background-item-1"></div>
        <div class="background-item-2"></div>
        <div class="background-item-3"></div>
    </section>
    <section id="front-catalog" class="relative" data-anchor="katalog">
        <div class="dis-flex justify-content-center">
            <div class="col-lg-9 col-md-11 col-xs-11">
                <div class="katalog-filter margin-b-20">
                    <ul class="dis-flex flex-wrap-wrap justify-content-center text-center">
                    <?php 
                    $categories = get_terms(array(
                        'taxonomy' => 'product_cat',
                        'hide_empty' => false,
                    ));
                    $active = get_query_var('product_cat');
                    foreach($categories as $cat){
                        if ($cat->slug == 'uncategorized'){
                            echo '<li type="button" class="'.($active == '' ? 'active' : '').'" data-filter="all">Все</li>';
                            continue;
                        }
                        echo "<li type='button' class='".($active == $cat->slug ? 'active' : '')."' data-filter='.product_cat-{$cat->slug}'>{$cat->name}</li>";
                    }
                    ?>
                    </ul>
                </div>
                
                <div class="katalog-wrap margin-t-40 dis-grid grid-col-lg-3 grid-col-xs-1 grid-row-40">
                    <?php
                    
                    if ( wc_get_loop_prop( 'total' ) ) {
                        while ( have_posts() ) {
                            the_post();

                            /**
                             * Hook: woocommerce_shop_loop.
                             *
                             * @hooked WC_Structured_Data::generate_product_data() - 10
                             */
                            do_action( 'woocommerce_shop_loop' );

                            wc_get_template_part( 'content', 'product' );
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="background-item-1"></div>
        <div class="background-item-2"></div>
    </section>
    <section id="front-about" class="relative">    
        <div class="dis-flex flex-wrap-wrap section-margin-top align-items-center">
            <div class="col-lg-vw-6">
                <img src="<?=get_stylesheet_directory_uri()?>/dest/images/front/about-image.png">
            </div>
            <div class="col-lg-vw-4 text-block">
                <div class="section-title big margin-b-30">
                    <h2>Хлебные крошки</h2>
                    <small>Что это и зачем?</small>
                </div>
                <p>Когда вы последний раз ели вкусный хлеб? Не, ну серьездно?
                Все эти сетевые магазины заполонили рынок и торгуют либо сухим, либо резиновым хлебом с химикатами. А купить свежий хлеб утром, чтобы есть его в течении дня, вообще нереальная задача. Это надо проснуться на час раньше, одеться, выйти из дома съездить до пе- карни, вернуться обратно и только потом можно завтракать или скорее обедать. Жесть! На это нет времени и никакого желания.</p>
                <p>Я решил эту проблему, создав сервис по доставке горячего, нату- рального хлеба высшего качества прямо до дома. Мы привозим хлеб из пекарни в удобное для вас утреннее время, чтобы вы и ваша семья наслаждалась вкусом свежего хлеба с хрустящей короч- кой на завтрак обед и ужин.</p>
                <blockquote>Денис Левченко. Создатель хлебных крошек</blockquote>
            </div>
        </div>
        <div class="dis-flex flex-direction-col align-items-center">
            <div class="how-it-works col-lg-6 col-md-8 col-xs-11 section-margin-top">
                <div class="section-title margin-b-20 text-center big">
                    <h2>Как это работает?</h2>
                </div>
                <div class="dis-grid grid-row-40 grid-col-lg-3 grid-col-xs-1 text-center">
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-32.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-33.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-34.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-35.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-36.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-37.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                    <div class="how-it-works-item">
                        <img src="<?=get_stylesheet_directory_uri()?>/dest/images/icons/hlebnie-kroshki-icon-38.svg">
                        <p>Выбираете понравившийся вам хлеб</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php
get_footer( 'shop' );
