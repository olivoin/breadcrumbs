<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<div <?php post_class(array('katalog-item','mix'))?>>
    <div class="katalog-item-image">
        <?php the_post_thumbnail(); ?>
    </div>
    <div class="katalog-item-desc">
        <div class="katalog-item-desc-title">
            <h3><?=get_the_title();?></h3>
        </div>
        <div class="katalog-item-desc-attr">
            <div class="katalog-item-desc-attr-type">Белый</div>
            <div class="katalog-item-desc-attr-weight">380г</div>
            <div class="katalog-item-desc-attr-leight">38см</div>
        </div>
    </div>
    <?=do_action( 'woocommerce_after_shop_loop_item' );?>
</div>
