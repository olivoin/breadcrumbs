<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=1366">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<link rel='stylesheet' href='<?=get_stylesheet_directory_uri()?>/dest/css/production.min.css' type='text/css' media='all' />
</head>

<body <?php body_class(); ?>>

<header id="header">
    <div class="dis-flex justify-content-center">
        <div class="col-lg-11 dis-flex flex-wrap-wrap align-items-center justify-content-center">
            <div class="col-lg-2">
            <div class="header-logo">
                <a href="#"></a>
            </div>
        </div>
            <div class="col-lg-7">
            <div class="dis-flex flex-wrap-wrap align-items-center">
                <div class="col-lg-4">
                    <?php 
                    echo wp_nav_menu(
                        array(
                            'theme_location'  => 'primary',
                            'menu_class'      => 'dis-flex flex-wrap-wrap justify-content-end',
                            'container'       => 'nav',
                            'container_class' => 'header-nav'
                            )
                    );
                    ?>
                </div>
                <div class="header-subscribe col-lg-4 text-center">
                    <a href="<?=home_url();?>" class="button">хотеть хлеб ежедневно!</a>
                </div>
                <div class="col-lg-4">
                    <div class="header-phone">
                        <a href="" class="company_phone">8 800 200 000 02</a>
                    </div>
                </div>
            </div>
        </div>
            <div class="col-lg-3">
            <div class="header-meta relative dis-flex flex-wrap-wrap align-items-center justify-content-end">
                <?php if (!is_user_logged_in()): ?>
                <div class="header-meta-cart margin-r-20">
                    <?php the_widget( 'WC_Widget_Cart', 'title=' ); ?>
                </div>
                <?php endif; ?>
                <div class="header-meta-kabinet margin-l-20">
                    <a href="<?=esc_url( wc_get_page_permalink( 'myaccount' ) )?>">
                        <?php if (is_user_logged_in()) { 
                        global $current_user; wp_get_current_user();
                          echo '' . $current_user->user_firstname . "\n";
                          echo '' . $current_user->user_lastname . "\n";
                    } else {
                        echo 'Войти';
                    } ?>
                    <?php wp_loginout(); ?>
                    </a>
                </div>
            </div>
        </div>
        </div>
    </div>  
</header>

<?php 
$category_page = get_query_var('product_cat');
if (!is_front_page() && !$category_page): ?>
<main>
    <section id="profile" class="section-padding-top section-padding-bottom">
        <div class="dis-flex justify-content-center">
            <div class="profile-wrap col-lg-8 col-md-10">
<?php endif; ?>